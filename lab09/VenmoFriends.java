///Skylar,Will, Mike    Group I



import java.util.Scanner;
public class VenmoFriends {

  public static void main(String [] args){
    Scanner scnr = new Scanner(System.in);
    int choice;
    double balance = 100;
    double money = 0;
    String friend;
    
    String [] name = new String[5];
    name[0] = "Mike";
    name[1] = "Will";
    name[2] = "Skylar";
    
    int index = 2; 
    double [] moneySent = new double[5];
    double [] moneyReceived = new double[5];
    
    moneySent[0] = 100;
    moneySent[1] = 1;
    moneySent[2] = 20;
    
    moneyReceived[0] = 40; 
    moneyReceived[1] = 41; 
    moneyReceived[2] = 3;
    
    
    do{
      printMenu();
      choice = getInt();   
      switch (choice){
        case 1: getBalance(balance);
                balance = sendMoney(balance, name);    
                break;
        case 2: balance = requestMoney(balance, name, index);
                break;
        case 3: getBalance(balance);
                break;
        case 4: System.out.println("Goodbye");
                break;
        case 5: printReport(scnr, name, index, moneySent, moneyReceived);
                break;

        case 6: index++;
                name = addFriends(scnr, name, index);
                System.out.println("New Friend List:");
                for(int i= 0; i <= index; i++)
                    System.out.println(name[i]);
                index++;
                break;   
        default: System.out.println("you entered an invalid value -- try again");
                break;
      }
    }while(choice != 4);
  }
  
  
  //print Venmo Main Menu
  public static void printMenu(){
      System.out.println("Venmo Main Menu");
      System.out.println("1. Send Money");
      System.out.println("2. Request Money");
      System.out.println("3. Check Balance");
      System.out.println("4. Quit");
      System.out.println("5. Print Report");
      System.out.println("6. Add Friend");
  }
  
  //get a number and check for an integer
  public static int getInt(){
    Scanner input = new Scanner(System.in);
     while(input.hasNextInt() == false){
        System.out.println("You entered and invalid value -- try again");
        input.nextLine();//clear buffer
     }
     return input.nextInt();
  } 
  public static void getBalance(double balance){
     System.out.println("You have $" + balance + " in your account" );
  }
  
  //get a number and check for a double
  public static double getDouble(){
    Scanner input = new Scanner(System.in);
    while(input.hasNextDouble() == false){
         System.out.println("You entered and invalid value -- try again");
         input.nextLine(); 
     } 
     return input.nextDouble();
  }
  
  
  
  public static double sendMoney(double balance, String[] list){
      double money = 0;
      System.out.println("How much money do you want to send?");
      money = getDouble();
      balance = checkMoney(money,balance, list);  
      return balance;
  }
  
  //prints friend and amount sent to friend
  public static void getFriend(double money, String[] list){
        Scanner input = new Scanner(System.in);
        System.out.println("Who do you want to send your money to?");
        String friend = input.nextLine();
        for(int i=0;i<list.length; i++) {
            if(list[i].equals(friend))
                System.out.println("You have sent "+ money +" to "+ friend);
            else return; 
        }
  }
  
  
  public static double checkMoney(double money, double balance, String[] list){
        while (money <= 0){
            System.out.println("Invalid amount entered");
            money = getDouble();
         }
         if (money <= balance){
             balance -= money;
             getFriend(money, list);
           }
          else{
             System.out.println("You do not have sufficient funds for this transaction.");
          }
          return balance;
  }
  
  
  public static double requestMoney(double balance, String[] list, int index){
     System.out.println("How much money do you want to request?");
     double money = getDouble(); 
     balance = checkRequestMoney(money,balance, list, index);
     System.out.println("Once confirmed, your new balance will be $" + balance);
     return balance;
               
}
  
  //gets friends information for the Request menu item and prints the amount requested from friend
  public static boolean getFriendRequest(double money, String[] list, int index){
     Scanner input = new Scanner(System.in);
     System.out.println("Who do you want to request money from?");
     String friend = input.nextLine(); 
     for(int i=0;i<index; i++) {
            if(list[i].equals(friend)) {
                System.out.println("You are requesting $" + money + " from "+ friend);
                return true;
            }      
     }
       System.out.print("qwerty");
                return false;

     
  }
  
  //method makes sure money is greater than 0 and adds requested amount to balance and returns new balance
  public static double checkRequestMoney(double money, double balance, String[] list, int index) {
      boolean check; 
      while (money <= 0){
           System.out.println("Invalid amount entered");
           money = getDouble();
      }
      check = getFriendRequest(money, list, index);
      if (check == true) {
            balance += money;
      }
      else {
          return balance +=0;
      }
 
      return balance;
  }
  
  //add friends method 
  public static String[] addFriends(Scanner scnr, String[] name, int index) {
          System.out.print("Enter name of a user: ");
          int i = index;
          name[i] = scnr.nextLine();   
          return name; 
          
  }
  
  public static void printReport(Scanner scnr, String[] name, int index, double[] sent, double [] rcvd) {
          for(int i = index; i < name.length; i++) {
              if(name[i] == null)
                  break;
          }
          int j = index; 
          sent[j] = 0.0; 
          for(j = index; j < name.length; j++) {
               if(sent[j] == 0.0)
                  break;
          }
          int k = index; 
          rcvd[k] = 0.0;
          for(k = index; k < name.length; k++) {
               if(rcvd[k] == 0.0)
                  break;
          }
          
          System.out.println("Friend List\tMoney Sent\tMoney Received");
          for(int h= 0; h <= index; h++)
                System.out.println(name[h] + "\t\t" + sent[h] + "\t\t" + rcvd[h]);
  }

  
  
  
}   
