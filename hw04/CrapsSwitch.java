//Skylar Schneider
//CSE 2-210
//9.25.18
//HW 4
//the purpose of this homework is to use switch statemnents and the scanner to generate random numbers, along with selection statments, operators and string manipulation
import java.util.Scanner;//importing scanner
public class CrapsSwitch //defining the class
{
 
  //main method required for every Java program
  public static void main(String[] args) 
  {
    
 int diceNumber1;//defining dicenumber1 as an int
 int diceNumber2;//defining dicenumber1 as an int
    
 Scanner myScanner = new Scanner ( System.in );//myscanner
 
    
 System.out.println("Would you like to randomly cast dice? (Answer Y or N)");//making the user say if they want the numbers randomly picked or not
 String answer = myScanner.next(); //the next string is the input
   
 if  (answer.equals("N")) { //if the user wants to pick the number
   System.out.println("Enter your first number: "); //asks to enter the first number
   diceNumber1 = myScanner.nextInt();//the next number that is inputted is the first number
   System.out.println("Enter your second number: "); //asks the user to print the other number
   diceNumber2 = myScanner.nextInt();//the next number that is inputted is the second number
 
   switch (diceNumber1){//switch the to dice number 1
  case 1: //if the dice equals 1
    switch (diceNumber2){//switch dice equals 2
      case 1://is the second dice equals the case number, then it tells you what to print
        System.out.println("Snake Eyes");
        break;
      case 2:
        System.out.println("Ace Deuce");
         break;
      case 3:
        System.out.println("Easy Four");
         break; 
      case 4:
        System.out.println("Fever Five");
          break;
      case 5:
        System.out.println("Easy Six");
        break;
      case 6:
        System.out.println("Seven out");
          break;
    }
       break;
  case 2://if dice 1 is equal to two
    switch (diceNumber2){//switch dice equals 2
      case 1://is the second dice equals the case number, then it tells you what to print
        System.out.println("Ace Deuce");
        break;
      case 2:
        System.out.println("Hard Four");
        break;
      case 3:
        System.out.println("Fever Five");
        break;
      case 4:
         System.out.println("Easy Six");
        break;
      case 5:
          System.out.println("Seven Out");
        break;
      case 6:
        System.out.println("Easy Eight");
        break; 
    
    }
       break;
  case 3: //if dice 1 is equal to 3
    switch (diceNumber2){ //if the second dice equals the case number, then it tells you what to print
      case 1:
        System.out.println("Easy Four");
        break;
      case 2:
        System.out.println("Fever Five");
        break;
      case 3:
        System.out.println("Hard Six");
      break;
      case 4:
        System.out.println("Seven Out");
        break;
      case 5: 
        System.out.println("Easy Eight");
        break;
      case 6:
        System.out.println("Nine");
        break;
     
   }
       break;
      
  case 4://and so on and so on.. like the other comments from before
    switch (diceNumber2){
      case 1:
        System.out.println("Fever Five");
        break;
      case 2:
        System.out.println("Easy Six");
        break;
      case 3:
        System.out.println("Seven Out");
        break;
      case 4:
        System.out.println("Hard Eight");
      break;
      case 5:
        System.out.println("Nine");
      break;
      case 6:
        System.out.println("Easy Ten");
        break;
       
    }
       break;
  case 5:
    switch (diceNumber2){
      case 1:
        System.out.println("Easy Six");
        break;
      case 2:
        System.out.println("Seven Out");
        break;
      case 3:
        System.out.println("Easy Eight");
        break;
      case 4:
        System.out.println("Nine");
        break;
      case 5:
        System.out.println("Hard Ten");
        break;
      case 6:
        System.out.println("Yo-leven");
        break;
        
    }   
       break;
  case 6:
    switch (diceNumber2){
      case 1:
        System.out.println("Seven Out");
        break;
      case 2:
        System.out.println("Easy Eight");
        break;
      case 3:
        System.out.println("Nine");
        break;
      case 4:
        System.out.println("Easy Ten");
        break;
      case 5:
        System.out.println("Yo-leven");
        break;
      case 6:
        System.out.println("Box Cars");
        break;
        
    }
       break;
} //end of not random
 }
   if (answer.equals("Y")) {//if the user wants the numbers to be random
   diceNumber1 = (int) (Math.random() * 6) + 1;//picks a random number from 1 to 6 inclusive
   System.out.println("Die number one is equal to: " + diceNumber1);
   diceNumber2 = (int) (Math.random() * 6) + 1;
   System.out.println("Die number two is equal to: " + diceNumber2);
   
    switch (diceNumber1){//same as the comments from before
  case 1:
    switch (diceNumber2){
      case 1:
        System.out.println("Snake Eyes");
          break;
      case 2:
        System.out.println("Ace Deuce");
          break;
      case 3:
        System.out.println("Easy Four");
          break;
      case 4:
        System.out.println("Fever Five");
          break;
      case 5:
        System.out.println("Easy Six");
          break;
      case 6:
        System.out.println("Seven out");
          break; 
    }
        break;
        
  case 2:
    switch (diceNumber2){
      case 1:
        System.out.println("Ace Deuce");
        break;
      case 2:
        System.out.println("Hard Four");
        break;
      case 3:
        System.out.println("Fever Five");
        break;
      case 4:
         System.out.println("Easy Six");
        break;
      case 5:
          System.out.println("Seven Out");
        break;
      case 6:
        System.out.println("Easy Eight");
        break;
    }
        break;
  case 3:
    switch (diceNumber2){
      case 1:
        System.out.println("Easy Four");
        break;
      case 2:
        System.out.println("Fever Five");
        break;
      case 3:
        System.out.println("Hard Six");
        break;
      case 4:
        System.out.println("Seven Out");
        break;
      case 5: 
        System.out.println("Easy Eight");
        break;
      case 6:
        System.out.println("Nine");
        break;
        
   }
        break;
  case 4:
    switch (diceNumber2){
      case 1:
        System.out.println("Fever Five");
        break;
      case 2:
        System.out.println("Easy Six");
        break;
      case 3:
        System.out.println("Seven Out");
        break;
      case 4:
        System.out.println("Hard Eight");
      break;
      case 5:
        System.out.println("Nine");
      case 6:
        System.out.println("Easy Ten");
        break;
   
    }
        break;
  case 5:
    switch (diceNumber2){
      case 1:
        System.out.println("Easy Six");
        break;
      case 2:
        System.out.println("Seven Out");
        break;
      case 3:
        System.out.println("Easy Eight");
        break;
      case 4:
        System.out.println("Nine");
        break;
      case 5:
        System.out.println("Hard Ten");
        break;
      case 6:
        System.out.println("Yo-leven");
        break;
       
    }   
        break;
  case 6:
    switch (diceNumber2){
      case 1:
        System.out.println("Seven Out");
        break;
      case 2:
        System.out.println("Easy Eight");
        break;
      case 3:
        System.out.println("Nine");
        break;
      case 4:
        System.out.println("Easy Ten");
        break;
      case 5:
        System.out.println("Yo-leven");
        break;
      case 6:
        System.out.println("Box Cars");
        break;
        
    }
        break;
}
    
    }
}//end the public class and main method
}