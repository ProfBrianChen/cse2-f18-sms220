//Skylar Schneider
//CSE 2-210
//9.25.18
//HW 4
//the purpose of this homework is to use if statements and the scanner to generate random numbers, along with selection statments, operators and string manipulation
import java.util.Scanner; //import the scanner
public class CrapsIf //defines the public class
{
 
  //main method required for every Java program
  public static void main(String[] args) 
  {
    
 int diceNumber1;//defines dice number one as an integer
 int diceNumber2;//defines dice number 2 as an integer
    
 Scanner myScanner = new Scanner ( System.in );//makes new scanner
 
    
 System.out.println("Would you like to randomly cast dice? (Answer Y or N)");//Asked if they want to randomly cast the numbers
 String answer = myScanner.nextLine(); //says that the next string is the input
   
 if  (answer.equals("N")) {//is the user says no
   System.out.println("Enter your first number: ");//prints to ask the user what the first number that they want is
   diceNumber1 = myScanner.nextInt();//the next number is the number that is used
   System.out.println("Enter your second number: ");//prints to ask the user what the second number that they want is
   diceNumber2 = myScanner.nextInt();//the next number is the number that is used
   
   if ((diceNumber1 == 1) && (diceNumber2 == 1)){ //if both die are equal to 1 then print snake eyes
     System.out.println("Snake Eyes");
   }

   else if (diceNumber1 == 1){ //if the first dice is equal to one
     if (diceNumber2 == 2){ //if the second dice is 2 then print ace deuce
       System.out.println("Ace Deuce");
     }
     else if (diceNumber2 == 3) {//if the second dice is equal to three then print what is in the print statement
       System.out.println("Easy Four");
     }
     else if (diceNumber2 == 4){//same for the rest 
       System.out.println("Fever Five");
     }
     else if (diceNumber2 == 5){
       System.out.println("Easy Six");
     }
     else if (diceNumber2 == 6){
       System.out.println("Seven Out");
     }
     }
    else if (diceNumber1 == 2) {
      if (diceNumber2 == 1){
        System.out.println("Ace Deuce");
      }
      if (diceNumber2 == 2){
        System.out.println("Hard Four");
      }
      else if (diceNumber2 == 3){
        System.out.println("Fever Five");
      }
      else if (diceNumber2 == 4){
        System.out.println("Easy Six");
      }
      else if (diceNumber2 == 5){
        System.out.println("Seven Out");
      }
      else if (diceNumber2 == 6){
        System.out.println("Easy Eight");
      }
    }
     else if (diceNumber1 == 3){
       if (diceNumber2 == 1){
         System.out.print("Easy Four");
       }
       if (diceNumber2 == 2){
         System.out.println("Fever Five");
       }
       if (diceNumber2 == 3){
         System.out.println("Hard Six");
       }
       else if (diceNumber2 == 4){
         System.out.println("Seven Out");
       }
       else if (diceNumber2 == 5){
         System.out.println("Easy Eight");
       }
       else if (diceNumber2 == 6){
         System.out.println("Nine");
       }
     }
     else if (diceNumber1 == 4){
       if (diceNumber2 == 1){
         System.out.println("Fever Five");
       }
       if(diceNumber2 == 2){
         System.out.println("Easy Six");
       }
       if (diceNumber2 == 3){
         System.out.println("Seven Out");
       }
       if (diceNumber2 == 4){
         System.out.println("Hard Eight");
       }
        else if (diceNumber2 == 5){
          System.out.println("Nine");
        }
       else if (diceNumber2 == 6){
         System.out.println("Easy Ten");
       }
     }
      else if (diceNumber1 == 5){
        if (diceNumber2 == 1){
         System.out.println("Easy Six");
       }
       if(diceNumber2 == 2){
         System.out.println("Seven Out");
       }
       if (diceNumber2 == 3){
         System.out.println("Easy Eight");
       }
       if (diceNumber2 == 4){
         System.out.println("Nine");
       }
        if (diceNumber2 == 5){
          System.out.println("Hard Ten");
       }
        else if (diceNumber2 == 6){
          System.out.println("Yo-leven");

        }
      }
     else if (diceNumber1 == 6){
       if (diceNumber1 == 6){
       System.out.println("Box Cars");
       }
        if (diceNumber2 == 1){
         System.out.println("Easy Seven");
       }
       if(diceNumber2 == 2){
         System.out.println("Easy Eight");
       }
       if (diceNumber2 == 3){
         System.out.println("Nine");
       }
       if (diceNumber2 == 4){
         System.out.println("Easy Ten");
       }
        if (diceNumber2 == 5){
          System.out.println("Yo-leven");
       }
       }
       

  } //done with if answer equals N
  
 else if (answer.equals("Y")) {//if the user says that they want to randomly cast
      diceNumber1 = (int) (Math.random() * 6) + 1;//picks a random number from 1 to 6 inclusive
      diceNumber2 = (int) (Math.random()* 6) + 1;
      System.out.println("Die number one is " + diceNumber1);
   System.out.println("Die number two is " + diceNumber2);
     
    if ((diceNumber1 == 1) && (diceNumber2 == 1)){//same as the if statements from before
     System.out.println("Snake Eyes");
   }

   else if (diceNumber1 == 1){
     if (diceNumber2 == 2){
       System.out.println("Ace Deuce");
     }
     else if (diceNumber2 == 3) {
       System.out.println("Easy Four");
     }
     else if (diceNumber2 == 4){
       System.out.println("Fever Five");
     }
     else if (diceNumber2 == 5){
       System.out.println("Easy Six");
     }
     else if (diceNumber2 == 6){
       System.out.println("Seven Out");
     }
     }
    else if (diceNumber1 == 2) {
      if (diceNumber2 == 1){
        System.out.println("Ace Deuce");
      }
      if (diceNumber2 == 2){
        System.out.println("Hard Four");
      }
      else if (diceNumber2 == 3){
        System.out.println("Fever Five");
      }
      else if (diceNumber2 == 4){
        System.out.println("Easy Six");
      }
      else if (diceNumber2 == 5){
        System.out.println("Seven Out");
      }
      else if (diceNumber2 == 6){
        System.out.println("Easy Eight");
      }
    }
     else if (diceNumber1 == 3){
       if (diceNumber2 == 1){
         System.out.print("Easy Four");
       }
       if (diceNumber2 == 2){
         System.out.println("Fever Five");
       }
       if (diceNumber2 == 3){
         System.out.println("Hard Six");
       }
       else if (diceNumber2 == 4){
         System.out.println("Seven Out");
       }
       else if (diceNumber2 == 5){
         System.out.println("Easy Eight");
       }
       else if (diceNumber2 == 6){
         System.out.println("Nine");
       }
     }
     else if (diceNumber1 == 4){
       if (diceNumber2 == 1){
         System.out.println("Fever Five");
       }
       if(diceNumber2 == 2){
         System.out.println("Easy Six");
       }
       if (diceNumber2 == 3){
         System.out.println("Seven Out");
       }
       if (diceNumber2 == 4){
         System.out.println("Hard Eight");
       }
        else if (diceNumber2 == 5){
          System.out.println("Nine");
        }
       else if (diceNumber2 == 6){
         System.out.println("Easy Ten");
       }
     }
      else if (diceNumber1 == 5){
        if (diceNumber2 == 1){
         System.out.println("Easy Six");
       }
       if(diceNumber2 == 2){
         System.out.println("Seven Out");
       }
       if (diceNumber2 == 3){
         System.out.println("Easy Eight");
       }
       if (diceNumber2 == 4){
         System.out.println("Nine");
       }
        if (diceNumber2 == 5){
          System.out.println("Hard Ten");
       }
        else if (diceNumber2 == 6){
          System.out.println("Yo-leven");

        }
      }
     else if (diceNumber1 == 6){
       if (diceNumber1 == 6){
       System.out.println("Box Cars");
       }
        if (diceNumber2 == 1){
         System.out.println("Easy Seven");
       }
       if(diceNumber2 == 2){
         System.out.println("Easy Eight");
       }
       if (diceNumber2 == 3){
         System.out.println("Nine");
       }
       if (diceNumber2 == 4){
         System.out.println("Easy Ten");
       }
        if (diceNumber2 == 5){
          System.out.println("Yo-leven");
       }
       }
       

  }
   }//end of the public class and main method
   }
