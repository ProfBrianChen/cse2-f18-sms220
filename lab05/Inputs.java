//Skylar Schneider
//10.3.2018
//CSE 002-210
//lab05
/*This lab gives practice with writing loops. Requires us to print an
error message when the user types in an unwanted word or an input with the 
wrong type*/
//two using the while and two using do while loop
import java.util.Scanner;

public class Inputs {
public static void main (String [] args){

int courseNum;
int meetTimes;
String deptName;
int classTime;
String instructorName;
int numStudents;
String junk = "";

Scanner myScanner = new Scanner (System.in);

System.out.println("What is the course number?");
while(!myScanner.hasNextInt()){
System.out.println("Please input an integer!");
 junk = myScanner.nextLine();
}
courseNum = myScanner.nextInt();
junk = myScanner.nextLine();

System.out.println("What is the department name?");
while(!myScanner.hasNextLine()){
System.out.println("Please input a string value!");

}
deptName = myScanner.nextLine();

  
System.out.println("What is number of meeting times?");
while(!myScanner.hasNextInt()){
System.out.println("Please input an integer value!");
 junk = myScanner.nextLine();
}
meetTimes = myScanner.nextInt();
junk = myScanner.nextLine();  

System.out.println("What is the time of the class?(don't include colons)");
while(!myScanner.hasNextInt()){
System.out.println("Please input an integer value!");
 junk = myScanner.nextLine();
}
classTime = myScanner.nextInt();
junk = myScanner.nextLine();  
  
System.out.println("What is the name of the instructor?");
while(!myScanner.hasNextLine()){
System.out.println("Please input a string value");
 
}
instructorName = myScanner.nextLine();
 
System.out.println("What is the number of students?");
while(!myScanner.hasNextInt()){
System.out.println("Please input an integer!");
 junk = myScanner.nextLine();
}
numStudents = myScanner.nextInt();
junk = myScanner.nextLine();  

System.out.println("Course Number is: " + courseNum);
System.out.println("Department Name is: " + deptName);
System.out.println("The number of meeting times is: " + meetTimes);
System.out.println("The time the class meets is: " + classTime);
System.out.println("The name of the instructor is: " + instructorName);
System.out.println("The number of students is: " + numStudents);
}
}