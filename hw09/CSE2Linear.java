//Skylar Schneider
//CSE 2-210
//HW 09
//// This homework gives you practice with arrays and in searching single dimensional arrays

import java.util.Scanner;//imports Scanner in order to have users input values
import java.util.Arrays;//imports Arrays in order to transform arrays..you can use this class to assign a value to one or more elements of an array.
public class CSE2Linear{//main method 

  public static void main (String[]args){//main methods
    System.out.println("Enter 15 final grades for CSE2");
    Scanner myScanner = new Scanner(System.in);
    int count = 0; //counter
    int [] A1 = new int[15];//array of length 15
     while (count < 15)// as long as counter is less than 15 loops
        {
            if (myScanner.hasNextInt()){// checks to see if user input is an integer
          
                A1[count] = myScanner.nextInt();// if so saves it as an integer
                if (A1[count] < 0 || A1[count] > 100){// checks to see if the user input is greater than 100 or less than 0
                
                    System.out.println("Error!! You didn't enter an int within the range 0-100");// if they did then the program ends
                    System.exit(0);
                }
                else{
                    if (count > 0)// if the count is greater than 0
                    {
                        if (A1[count] < A1[count - 1]){// check to make sure the latest input is greater than the previous number
                        
                            System.out.println("You didn't enter an intger greater than the last one ");// if not then prints error and ends program
                            System.exit(0);
                        }
                    }
                count++;// increments the counter
                }
            }
            else{ // since input isn't an int
            
                System.out.println("You didn't enter an integer");// prints error message and ends program
                System.exit(0);
            }
        }
        System.out.println("Sorted: ");// prints "Sorted: "
        System.out.println(Arrays.toString(A1));// converts array to a string statement to be printed out
        System.out.print("Enter a grade to search for: ");// tells user to enter a grade to search for
        if (myScanner.hasNextInt()){// checks to see if the user input an int
            int x = myScanner.nextInt();// if integer was input saves as int
            BinarySearch(A1, x);// calls on LinearSearch Method
            Scramble(A1);// calls on Scramble Method
            System.out.println("Scrambled:");// prints "Scrambled:"
            System.out.println(Arrays.toString(A1));// coverts scrambled array to string statement to be printed out
            System.out.print("Enter a grade to search for: ");// tells the user to enter a number to search for
            if (myScanner.hasNextInt()){// check to see if the user input an int
              int y = myScanner.nextInt();
                LinearSearch(A1,y);// if user entered an int calls on LinearSearch Method
            }
            else{// else prints error message and ends program
           
                System.out.println("You didn't enter an integer");
                System.exit(0);
            }
        }
        else // if not then prints an error and ends program
        {
            System.out.println("You didn't enter a grade to seach for");
            System.exit(0);
        }
    }
    
    public static void LinearSearch(int[] A1, int x)// LinearSearch method
    {
        for (int i = 0; i < A1.length; i++)// loops as long as int i is less than length of array 
        {
            if (A1[i] == x)// checks to see if the number at given position is equal to the user input
            {
                System.out.println(x + " was found in the list in " + (i + 1) + " iterations");// if so then prints where it was found
                break;
            }
            else if (A1[i] != x && i == (A1.length - 1))//if user input wasn't found after 15 iterations prints statement below
            {
                System.out.println(x + " was not found in " + (i + 1) + " iterations");
            }
        }
    }
    public static int[] Scramble(int[] A1){// Scramble method
        for (int i = 0; i < A1.length; i++){// loops for the length of the array 
            int x = (int)(Math.random()*A1.length);// random number 
            int store = A1[i];// temporarily stores number at i index in array
            while (x != i){// as long as the random number isn't equal to i switches index of i with index of random number
                A1[i] = A1[x];
                A1[x] = store;
                break;
            }
        }
        return A1;
    }
    public static void BinarySearch(int[] A2, int integer)//new method 
    {
        int high = A2.length - 1;//setting the highest index as the length minus 1
        int low = 0;//setting the lowest index to 0
        int count = 0;//setting the count equal to zero
        while (low <= high){//while the lowest index is less than the higher index
            count++;//incrementing the count
            int mid = (high + low)/2;//setting the mis equal to high plus low divided by two 
            if (integer < A2[mid])//if the integer is less than the mid index of A2
            {
                high = mid - 1;//change the value of the highest index equals mid minus 2
            }
            else if (integer > A2[mid]){//if the integer is greater than the mid index of A2
            
                low = mid + 1;//low index is equal to mid plus 1
            }
            else if (integer == A2[mid]){//else if statements
                System.out.println(integer + " was found in " + count + " iterations");
                break;
            }
        }
        if (low > high){
        
            System.out.println(integer + " was not found in " + count + " iterations");
        }
  }
}
