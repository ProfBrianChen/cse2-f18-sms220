//Skylar Schneider
//CSE2-210
//hw9
// This homework gives you practice with arrays and in searching single dimensional arrays

import java.util.Scanner;//imports Scanner in order to have users input values
public class RemoveElements
{
  public static void main(String [] arg)//code provided
  {
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
  	String answer="";
  	do
  	{
  	  System.out.print("Random input 10 ints [0-9]");
    	num = randomInput();
    	String out = "The original array is:";
    	out += listArray(num);
    	System.out.println(out);
    	
      System.out.print("Enter the index ");
      index = scan.nextInt();
      if (index < 0 || index > num.length - 1)//checks to see if index is between 0 and 9
      {
        System.out.println("Your number was not in the range 0-9");// if not doesn't call on range 
        System.out.println(out);
      }
      else 
      {
      newArray1 = delete(num,index);
    	String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
      }
   
      System.out.print("Enter the target value ");
    	target = scan.nextInt();
    	newArray2 = remove(num,target);
    	String out2="The output array is ";
    	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
    	System.out.println(out2);
        	 
    	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
    	answer=scan.next();
  	}
      while(answer.equals("Y") || answer.equals("y"));
  }
   
  public static String listArray(int num[])
  {
    String out="{";
    for(int j=0;j<num.length;j++){
    	if(j>0){
        out+=", ";
    	}
  	  out+=num[j];
    }
    out+="} ";
    return out;//the above code was provided
  }
  
  public static int[] randomInput()// randomInput Method
  {
    int number[]=new int[10];//creating new array with the length of 10
    for (int i = 0; i < number.length; i++){//make every index of the array a random number
      int x = (int)(Math.random()*10);
      number[i] = x;
    }
    return number;
  }
  
  public static int[] delete(int[]A1, int y)//new method teh purpose of this method is to delete the index that the user gives
  {
    int [] A2 = new int[9];// made a new array of length 9 
    for (int j = 0; j < y; j++){// copies all of the original array until the condition in the for loop is not met
    
      A2[j] = A1[j];//sets the two array lengths equal
    }
    for (int i = y + 1; i < A1.length; i++){// once indicated index is reached, doesn't include that number and puts all other from original array to the new array
      A2[y] = A1[i];
      y++;
    }
    return A2;//returns the second array in this method
  }
  
  public static int[] remove(int[]A1, int y){//new method created the purpose of this method is to remove the target value in the array that is entered by the user 
    int counter = 0;//set the counter equal to zero
    for (int i = 0; i < A1.length; i++){//for loop to go through the array and compare the input to the indeces
      if (A1[i] != y){//if the index in A1 is not equal to y
        counter++;//increment the counter
      }
    }
    int A2[] = new int [counter];//new array called counter
    int counter2 = 0;//sets the second counter equal to zero
    for (int j = 0; j < A1.length; j++){//for loop
      if (A1[j] != y){
      A2[counter2] = A1[j];
      counter2++;//increments counter
      }
    }
    return A2;//returns a2 in the method
  }
}