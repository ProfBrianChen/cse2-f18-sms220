//Skylar Schneider lab02 CSE2-210 9/5/18
//print the number of minutes,number of counts,distance in miles for each trip and then both trips combined

public class Cyclometer {
  //main method required for every Java program
  public static void main(String[] args) {

int secsTrip1 = 480;
//declaring variable number of seconds for trip 1 as an integer and assigning it a number;
int secsTrip2 = 3220;
//declaring variable number of seconds for trip 2 as an integer and assigning it a number;
int countsTrip1 = 1561;
//declaring variable number of rotations for trip 1 as an integer and assigning it a number;
int countsTrip2 = 9037;
//declaring variable number of rotations for trip 2 as an integer and assigning it a number;

double wheelDiameter = 27.0,
//declaring the varuable of the diameter of the wheel as a double and assigning it a number;
PI = 3.14159,
//setting assigning pi to a number
feetPerMile = 5280,
//declaring feet per mile as a double by using the comma and then assigning it a number
inchesPerFoot = 12,
//declaring the variable inches per foot as a double with the use of the comma from before and then assigning it a number
secondsPerMinute = 60;
//declaring the variable seconds per minute and then assigning it a number
double distanceTrip1, distanceTrip2, totalDistance;
//declares all of these variables as doubles

System.out.println("Trip 1 took "+
                  (secsTrip1/secondsPerMinute)+" minutes and had "+
                   countsTrip1+" counts.");
//printing how long trip1 took by calculating how many minutes and then the amount of counts.
System.out.println("Trip 2 took "+
                  (secsTrip2/secondsPerMinute)+" minutes and had "+
                   countsTrip2+" counts.");
//printing how long trip1 took by calculating how many minutes and then the amount of counts.
//my results were that trip1 took 8.0 minutes and had 1561 counts and trip 2 took 53.666666666666664 minutes and had 9037 counts.
    
distanceTrip1 = countsTrip1 * wheelDiameter * PI;
//calculating distance in miles 
//counts in the trip times the wheel diameter times pi
distanceTrip1 /= inchesPerFoot * feetPerMile; //distance in miles
distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
totalDistance = distanceTrip1 + distanceTrip2;

//print out the data
System.out.println("Trip 1 was "+distanceTrip1+" miles");
System.out.println("Trip 2 was "+distanceTrip2+" miles");
System.out.println("The total distance was "+totalDistance+" miles");
//my results were that Trip 1 was 2.0897820980113635 miles and Trip 2 was 12.098245240056817 miles
//the total distance was 14.188027338068181 miles
    
  } //end of main method
} //end of class

