//Skylar Schneider 
// 9/12/18 
//CSE 2 210
/*This lab session will demonstrate how you can get
input from the user and use that data to perform
basic computations*/

//In order to use the scanner class, you must import it 
import java.util.Scanner;

//adding the class and main method structure
public class Check{
  public static void main(String[] args) {
    //declaring checkCost as a double
    double checkCost;
    //in order to accept input, you must declare an instance of the Sanner object and call the Scanner constructor
    //tells the scanner that you are creating an instance that will take input from STDIN
    Scanner myScanner = new Scanner ( System.in );
   //printing this to prompt the user to give the cost of the check
    System.out.print ("Enter the original cost of the check in the form xx.xx: ");
    //this statement accepts users input
    checkCost = myScanner.nextDouble ();
    
    //declaring tipPercent as a double
    double tipPercent;
    //printing this prompts the user to say what percent of the cost they would like to tip
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form of xx): " );
    //the statement accepts the users input
    tipPercent = myScanner.nextDouble ();
    //we want to convert this percentage to a decimal value
    tipPercent /= 100;
     
      //declaring the number of people as an integer
     int numPeople;
    //prompting the user for the number of people they went to dinner with
    System.out.print ("Enter the number of people who went out to dinner: ");
    //accepts the users input
    numPeople = myScanner.nextInt();
    
    //Prints out the output that the group needs to spend in order to pay the checkCost
    //declares total cost and cost per person as a double
    double totalCost;
    double costPerPerson;
    //declares dollars as an integer
    int dollars; //whole dollar amount of cost dimes,pennies;
    //calculates the total cost by multiplying the check cost by 1 plus the tip percent
    totalCost = checkCost * (1 + tipPercent);
    //cost per person is the total cost divded by number of people
    costPerPerson = totalCost / numPeople; 
    //making dollars equal to the cost per person as an integer
    dollars = (int) costPerPerson;
    //getting the amount of dimes
    int dimes = (int) (costPerPerson * 10) % 10;
    //getting the amount of pennies
    int pennies = (int)(costPerPerson * 100) % 10;
    //prints the amount of dollars then number of dimes then number of pennies
    System.out.println("Each perosn in the group owes $" + dollars + '.' + dimes + pennies);    
    
    
    
    
    
    
  }//end of main method
}//end of class