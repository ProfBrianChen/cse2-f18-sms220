//Skylar Schneider
//CSE 002-210
//lab 04
//9.18.18
/*This lab session is an exercise in using if statements, switch statements and in using 
Math.random(), a random number generator*/

 //importing the random number generator
public class CardGenerator 
{
 
  //main method required for every Java program
  public static void main(String[] args) 
  {
 //declaring suit as a string and initializing it to anything that is in quotes
 String suit = "";
 // declaring card number as an integer
 int cardNumber;
 //saying that the card number is any random number between 1 and 52 inclusive
 cardNumber = (int) (Math.random() * 51 ) + 1;
 //declaring cardnumber1 as an int and setting it to cardnumber
 int cardNumber1 = cardNumber;
  
  //says that if the randomly picked card number is between 1-13, the suit is a diamond
 if ((cardNumber >= 1) && (cardNumber <=13 )){
   suit = "Diamonds";//explained above
   cardNumber1 = cardNumber;//setting card number 1 equal to cardnumber
 }
  //says that if the randomly picked card number is between 14 and 26, then the suit is a club
 if ((cardNumber>=14) && (cardNumber<=26)){
   suit ="Clubs";
   //sets card number 1 equal to cardnumber minus 13 so that it gives a number between 1 and 13
   cardNumber1 = cardNumber - 13;
 }
 //says that if the randomly picked card number is between 27 and 39, then the suit is a heart
 if ((cardNumber>=27) && (cardNumber<=39)){
   suit = "Hearts";
   //sets card number 1 equal to cardnumber minus 26 so that it gives a number between 1 and 13
   cardNumber1 = cardNumber - 26;
 }
  //says that if the randomly picked card number is between 40 and 52, then the suit is a spade   
 if ((cardNumber>=40) && (cardNumber<=52)){
   suit = "Spades";
  //sets card number 1 equal to cardnumber minus 39 so that it gives a number between 1 and 13
   cardNumber1 = cardNumber - 39;
 }
 
  String cardNumber2= "";//declares cardnumber2 as a string
  switch (cardNumber1){ //with all of the cases, switch cardnumber1
    case 1://if card number one is a 1,
    cardNumber2 = "Ace"; //switch it to an ace
    break;
  
    case 2://if card number one is a 2,
    cardNumber2 = "2"; //make it 2
    break;
    
    case 3:
    cardNumber2 = "3"; //same as for case 2
    break;
    
    case 4:
    cardNumber2 = "4";// ""
    break;
    
    case 5:
    cardNumber2 = "5";// ""
    break;
   
    case 6:
    cardNumber2 = "6";// ""
    break;
    
    case 7:
    cardNumber2 = "7";// ""
    break;
    
    case 8:
    cardNumber2 = "8";// ""
    break;
    
    case 9:
    cardNumber2 = "9";// ""
    break;
    
    case 10:
    cardNumber2 = "10";// ""
    break;
    
    case 11://if card number one is a 11
    cardNumber2 = "Jack";//switch it to an jack
    break;
    
    case 12://if card number one is a 12
    cardNumber2 = "Queen"; //switch it to an queen
    break;
    
    case 13://if card number one is a 13
    cardNumber2 = "King";//switch it to an king
    break;
  }//end of the switch statement
  
    
    
    
    
  System.out.println("You picked the " + cardNumber2 + " of " + suit);
    //print statement

    
    
    
     } //end of main method
    } //end of class