//Skylar Schneider
//10.22.2018
//CSE 002-210
//hw06
//The purpose of this lab is to get famliar with nested loops.


import java.util.Scanner;//importing scanner

public class EncryptedX {//defining the class
public static void main (String [] args){
  
 int i = 0; //declaring integer i has 
 String junk = "";//declaring a string as a buffer
  
 System.out.println("Please pick an integer between 1-100");
 Scanner myScanner = new Scanner (System.in); //naming the new scanner as myScanner
 while (myScanner.hasNextInt() == false){//loop  if the next input is not an integer
  System.out.println("Please enter an integer between 1 and 100!!");//printing to the user that the number that they entered is an error
  junk = myScanner.nextLine(); //clearing the buffer
}
 
  i = myScanner.nextInt();//the next integer put in is going to be the value of integer
  junk = myScanner.nextLine();//clearing the buffer
  
    if (i < 1) {
    System.out.println("Please enter an integer between 1 and 100!!");//the error message will print
  }
 else if (i>100){
  System.out.println("Please enter an integer between 1 and 100!!");//the error message will print
  System.exit(0);

  }
 
  
  //x represents the number or rows and y represents the number of columns
  for (int x=0; x<=i; ++x){//for loop that shows the amount of rows to print
    for(int y=0; y<=i; ++y){//nested for loop that prints the amount of columns
      if((x==y) || (x+y)==i) {
        System.out.print(" ");
      }
      else{//if the above conditions are not met then print a *
      System.out.print('*');
      }
    
    }
    System.out.println();
  }
  
  
 
}
}