//Skylar Schneider
//Group I
//ReportDo
//I worked on this alone which is why I am still having some errors. My group has not been working as a group should unfortunately.
import java.util.Scanner;
public class DoWhileExample{
  public static void main(String [] args){
    Scanner input = new Scanner(System.in);
    
    int choice = 0; 
    
    double balance = 0;
    double recieved = 0;
    double sentMoney = 0;
    String user = ""; //who they want to send money to 
    
    System.out.print("Please enter the initial balance of your account:");
    balance = input.nextDouble(); //gets initial bal of account frm user 
    
 
    
    do{
      System.out.println("Venmo Main Menu");
      System.out.println("1. Send Money"); 
      System.out.println("2. Request Money");
      System.out.println("3. Check Balance");
      System.out.println("4. Quit");

      choice = input.nextInt(); 
      
      if(choice == 1)
      {
        System.out.print("Who do you want to send your money to? ");
        user = input.next(); 
        
       do{
         System.out.print("Enter amount to send to " + user + "\n"
                          + "must be less than/equal to " + balance + "): $");
            
            while(!input.hasNextDouble()) 
            {
                System.out.print("Error, enter a valid dollar amount: "); 
                input.next(); 
            }
            sentMoney = input.nextDouble(); 

        }while(sentMoney > balance); 
       
        balance = balance - sentMoney; 
        System.out.println("$" + sentMoney + " to " + user);
        System.out.println("New Balance: " + balance);
       }
          
      choice = 4; 

    }
    while(choice != 4);
  }
}
