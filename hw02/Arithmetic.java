//Skylar Schneider hw02 CSE2-210 9/11/18
//This homework has the objective of giving you practice manipulating data stored in variables, in running simple calculations, and in printing the numerical output that you generated.

import java.lang.Math;
  
  public class Arithmetic 
{
 
  //main method required for every Java program
  public static void main(String[] args) 
  {
    //Declaring the integer, number of pairs of pants, as 3
    int numPants = 3;
    //Declaring the double, cost per pair of pants, as 34.98
    double pantsPrice = 34.98;
    
    //Declaring the integer, number of sweatshirts, as 2
    int numShirts = 2;
    //Declaring the double, cost per pair of shirts, as 24.99
    double shirtPrice = 24.99;
    
    //Declaring the integer, number of belts, as 1
    int numBelts = 1;
    //Declaring the double, cost per belt, as 33.99 
    double beltCost = 33.99;
    
    //declaring the double the tax rate as 0.06
    double paSalesTax = 0.06;

    //declaring total cost of pants, shirts and belts as a double
    double totalCostOfPants;
    double totalCostOfShirts;
    double totalCostOfBelts;
   
    
    //total cost of each kind of item
    //total cost of pants
    totalCostOfPants = (double) numPants * pantsPrice;
    //rounding to two decimal places
    totalCostOfPants = Math.floor(totalCostOfPants * 100) / 100;
    //total cost of shirts
    totalCostOfShirts = (double) numShirts * shirtPrice;
    //rounding to two decimal places
    totalCostOfShirts = Math.floor(totalCostOfShirts * 100) / 100;
    //total cost of Belts
    totalCostOfBelts = (double) numBelts * beltCost;
    //rounding to two decimal places
    totalCostOfBelts = Math.floor(totalCostOfBelts * 100) / 100;
    
    //Sales tax for each item
    //Total cost of the pants multiplied by the sales tax
    double pantsTax = totalCostOfPants * paSalesTax;
    //rounds to two decimal places
    pantsTax = Math.floor(pantsTax * 100) / 100;
    //Total cost of the shirts multiplied by the sales tax
    double shirtsTax = totalCostOfShirts * paSalesTax;
    //rounding to two decimal places
    shirtsTax = Math.floor(shirtsTax * 100) / 100;
    //Total cost of the belts multiplied by the sales tax
    double beltsTax = totalCostOfBelts * paSalesTax;
    //rounding to two decimal places
    beltsTax = Math.floor(beltsTax * 100) / 100;
    
    //declaring the total cost as a double
    double totalCost;
    //Calculating the total cost of purchases by added all of the total costs before taxes
    totalCost = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    //rounding to two decimal places
    totalCost = Math.floor(totalCost * 100) / 100;
    
    //declaring total sales tax as a double
    double totalSalesTax;
    //caluculating the total sales tax
    totalSalesTax = pantsTax + shirtsTax + beltsTax;
    //rounding to two decimal places
    totalSalesTax = Math.floor(totalSalesTax * 100) / 100;
    
    //declaring total paid as a double
    double totalPaid;
    //total paid for this transactions is the total cost plus the total sales tax
    totalPaid = totalSalesTax + totalCost;
    
    //prints out total cost of each item, the tax of each item, and then the total sales tax and the total paid
    System.out.println(totalCostOfPants);
    System.out.println(totalCostOfShirts);
    System.out.println(totalCostOfBelts);
    System.out.println(pantsTax);
    System.out.println(shirtsTax);
    System.out.println(beltsTax);
    System.out.println(totalCost);
    System.out.println(totalSalesTax);
    System.out.println(totalPaid);
    


   
    
    
      } //end of main method
} //end of class