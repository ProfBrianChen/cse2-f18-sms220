 ///CSE 02-210 Welcome Class HW 01
//Skylar Schneider 9/4/2018
public class WelcomeClass
{
  public static void main(String args [])
  {
    System.out.println("    ----------- ");
    System.out.println("    | WELCOME | ");
    System.out.println("    ----------- "); 
    System.out.println("   ^  ^  ^  ^  ^  ^ ");
    System.out.println("  / \\/ \\/ \\/ \\/ \\/ \\  ");
    System.out.println("  <-S--M--S--2--2--0->");
    System.out.println("  \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("   v  v  v  v  v  v "); 
    System.out.println("Hello my name is Skylar Schneider and I am a Junior with a BIS major and a CS Minor. I am from Long Island in New York. My favorite things to do in my free time are hang out with my friends and go to the gym.");
    
  }
  
}