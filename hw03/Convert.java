//Skylar Schneider
//HW 3 CSE 002-210
//9-17-2018
//Program 1


/* This homework teaches us to use java inorder to ask users for values 
and convert them into different units*/

//In order to use the scanner class, you must import it 
import java.util.Scanner;
//adding the class and main method structure
public class Convert{
    public static void main(String[] args) {
//in order to accept input, you must declare an instance of the Scanner object and call the Scanner constructor
    //tells the scanner that you are creating an instance that will take input from STDIN
Scanner myScanner = new Scanner ( System.in );
double acres;
//Printing out the statement that asks for the users input
System.out.println("Enter the affected area in acres: ");
//this statement accepts users input
acres = myScanner.nextDouble ();  
double rainfall;    
//Printing out the statement that asks for the users input
System.out.println("Enter the rainfall in the affected area: ");
//this statement accepts users input
rainfall = myScanner.nextDouble ();  
//converts into acre inches by multiplying acres and inches of rainfall
double acreInch = acres * rainfall;
//converts acre inches into gallons
double gallons = acreInch * 27154.2857;
//multiply the gallons by the given conversion constant
double rainfallCubicMiles = gallons * 9.08169e-13;
System.out.println(rainfallCubicMiles + " cubic miles.");
    }
 }