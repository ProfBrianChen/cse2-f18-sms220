//Skylar Schneider
//HW 3 CSE 002-210
//9-17-2018
//Program 2


/* This homework teaches us to use java inorder to ask users for values 
and convert them into different units*/

//In order to use the scanner class, you must import it 
import java.util.Scanner;
//adding the class and main method structure
public class Pyramid{
    public static void main(String[] args) {
//in order to accept input, you must declare an instance of the Scanner object and call the Scanner constructor
    //tells the scanner that you are creating an instance that will take input from STDIN
Scanner myScanner = new Scanner ( System.in );

//declares the length of the square side of the pyramid and the height of the pyramid as an int
int squareSide;
int height;
//asking the user to input the square side of the pyramid
System.out.println("The square side of the pyramid is (input length): ");
//the next integer that is inputed will be used as the square side of the pyramid
squareSide = myScanner.nextInt ();
//asking the user to input the height of the pyramid
System.out.println("The height of the pyramid is (input height): ");
//the next integer that the user inputs will be used as the height of the pyramid
height = myScanner.nextInt ();
//declaring volume of the pyramid
int volume;
//volume is equal to the square side squared multiplied by the height all over three      
volume = ((int)(Math.pow(squareSide,2) * height)/3);
//printing the final statement which is the volume of the pyramid
System.out.println("The volume inside the pyramid is: " + volume + ".");
    }
}



      


