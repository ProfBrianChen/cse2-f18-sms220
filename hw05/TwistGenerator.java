//Skylar Schneider
//10.9.2018
//CSE 002-210
//hw05
//The purpose of this lab is to get famliar with loops, a critical piece of syntax that is essential for many programming languages.The


import java.util.Scanner;//importing scanner

public class TwistGenerator {//defining the class
public static void main (String [] args){
  String top = "";//declaring variables
  String middle = "";
  String bottom = "";
  int mod = 1;
  int input = 25;
  Scanner myScanner = new Scanner (System.in);//naming the new scanner myScaner
  System.out.println("Please enter an integer.");
  input = myScanner.nextInt();//saying that input is equal to the value that the user inputs
  
  for (int i = 0; i < input; i++){//for loop saying to keep going through the loop as long as i is greater than the input
    if (mod == 1){
      top = top.concat("\\");//saying what to print for each row in the first line
      middle = middle.concat(" ");//saying what to print for each row in the middle line
      bottom = bottom.concat("/");//saying what to print for each row in the bottom line
    }
    if (mod == 2){
      top = top.concat(" ");
      middle = middle.concat("X");
      bottom = bottom.concat(" ");
      
    }
       if (mod == 3){
      top = top.concat("/");
      middle = middle.concat(" ");
      bottom = bottom.concat("\\");
       }
    mod++;
      if (mod == 4)
        mod = 1;
    
  }
  System.out.println(top);//printing the top middle and bottom
  System.out.println(middle);
  System.out.println(bottom);
}
}