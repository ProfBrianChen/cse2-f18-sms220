//Skylar Schneider
//HW08
//creating arrays and calling methods 

import java.util.Scanner;

public class Shuffling{//class
public static void main(String[] args) {//main method
Scanner scan = new Scanner(System.in);//naming scanner
//suits club, heart, spade or diamond 
String [] suitNames={"C","H","S","D"};
String [] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};
String [] cards = new String[52]; 
String [] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 
System.out.println();
printArray(cards); 
shuffle(cards); 
 System.out.println("Shuffled: ");
printArray(cards); 
while(again == 1){ 
  hand = getHand(cards,index,numCards); 
  System.out.println("Hand: ");
  printArray(hand);
  index = index - numCards;
  System.out.println("Enter a 1 if you want another hand drawn"); 
  again = scan.nextInt(); 
}  
}
  
  public static void printArray(String [] list){
  System.out.println("Sample Output");
  for(int i=0; i<list.length; i++){//for loop that goes until the length of the list is complete
    System.out.print(list[i] + " ");//prints the array that is generated/sent to the method
  }
    System.out.println();
}
  public static void shuffle(String [] list){
   //shuffles the cards in the array
  int i = 0;
   
  String temp;
  while(i<50){
  int random = (int) (Math.random() * 51 ) + 1;//random generator from 1 to 52
  temp = list[0];
  list[0] = list[random];
  list[random] = temp;//all of the past three lines were pulling the value from the randomly generated number spot in teh array
  i++;//incremented
  
  }
   System.out.println();//printing a new line
    
  }
  public static String [] getHand(String [] list, int index, int numCards){//a new method called getHand
  String[] getHand = {"","","","",""};//the number of spaces in the array
   for (int j = 0; j < numCards; j++) {//keep going in the for loops until the end of the length of the array
     getHand[j] = list[index - j];
  }
   
    
    return getHand;//returning get hand
    
  }
}


