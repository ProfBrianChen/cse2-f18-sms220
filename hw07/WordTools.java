//skylar schneider
//cse2-210
//hw 7
//This homework has the objective of giving you practice writing methods, manipulating strings and forcing the user to enter good input. 


import java.util.Scanner;
public class WordTools {
   // Creates an instance of Scanner class object
   public static Scanner scanner = new Scanner(System.in);


   public static String shortenSpace(String sampleString) {//creating the method for shortening the amount of spaces
       String shortSpace = sampleString.trim().replaceAll(" +", " ");//replacing more than one space with one space
       return shortSpace;//return

   }

 
   public static String replaceExclamation(String sampleString) {//creating the method to replace exclamation points with periods
       String replace = sampleString.replaceAll("!", ".");//saying to replace the ! with .
       return replace;
   }

   
   public static int findText(String sampleString, String find) {//the finding text method for finding the amount of instances of a word in a given text
       int count = 0;//setting the count to zero to correctly add of the number of times
       int i = 0;
      
       while ((i = sampleString.indexOf(find)) != -1) {//while loop so that it only loops through the amount of words that are in the sample string
           sampleString = sampleString.substring(i + find.length());
           count += 1;
       }
//adds up as you go through the loop
       return count;
   }

 
  public static int getNumOfWords(String sampleString){//counts the number of words in the sample string
    int count = 0;//setting the count and the total count equal to zero
    int totalCount = 0;
    for (int i = 0; i<sampleString.length(); i++){//loop to only loop for how big the word is
      if(sampleString.charAt(i) == ' '){//if there is a space, count is as a word
        count = count + 1;
        totalCount = count + 1;
      }
       }
    totalCount = count + 1;
    return totalCount;//counts the total number of words
    
  }

  
  public static int getNumOfNonWSCharacters(String sampleString){
    int count = 0;
    for (int i=0; i<sampleString.length(); i++){
      if (sampleString.charAt(i) != ' '){
        count = count + 1;
      }
  }
    return count;
}

 
   public static void printMenu() {
       System.out.println("Menu:");
       System.out.println("c - Number of non-whitespace characters");
       System.out.println("w - Number of words");
       System.out.println("f - Find text");
       System.out.println("r - Replace all !'s");
       System.out.println("s - Shorten spaces");
       System.out.println("q - Quit");
       System.out.println("Choose an option: ");
   }

 public static void main(String[] args) {

       while(true) {
           //prompt for the text
           System.out.println("Enter a sample text: ");
           String sampleString = scanner.nextLine();
  
           //Display user input
           System.out.println("You entered: " + sampleString);
  
           //Display menu
           printMenu();
           char input = scanner.nextLine().charAt(0);
  
           switch (input) {//using the switch method to call the different methods depending on teh user input
           case 'q':
               System.exit(0);
              
           case 'c':   //gets the Number of non-whitespace characters
               int cntNonWhitespaces = getNumOfNonWSCharacters(sampleString);
               System.out.println("Number of non-whitespace characters: " + cntNonWhitespaces);
               break;
              
           case 'w':   //finds the umber of words
               int wordsCount = getNumOfWords(sampleString);
               System.out.println("Number of words: " + wordsCount);
               break;
              
           case 'f':   //find text
               System.out.println("Enter a word or phrase to be found: ");
               String find = scanner.nextLine();
               int findCount = findText(sampleString, find);
               System.out.println( find +  "instances: " + findCount);
               break;
              
           case 'r':   //replace all !'s
               String replace = replaceExclamation(sampleString);
               System.out.println("Edited text: " + replace);
               break;
              
           case 's':   //shorten spaces
              
               System.out.println("Edited text:" + shortenSpace(sampleString));
               break;//prints out the edited text with the correct number of spaces
              
           default:
               System.out.println("Invalid option. Please try again");
           }
          
           System.out.println();
       }
   }
}